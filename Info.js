import React, {Component} from 'react';
import { StyleSheet, Text, View, Button, Linking,ActivityIndicator, WebView, AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import { State } from 'react-native-gesture-handler';

// function mapStateToProps() {
//   return {
//     lastPage: State.lastPage
//   }
// }

class InfoScreen extends Component {
  gameId = null;

  constructor(props) {
    super(props);

    const {navigation}  = this.props;
    this.gameId = navigation.getParam('gameId');
    this.getGameInfoFromApi()

    this.state = {
      game: {},
      loading: true
    }
    this.onPressGoBack = this.onPressGoBack.bind(this);
  }
  
  //Define your state for your component. 


  async getGameInfoFromApi() {
      try {
          //Assign the promise unresolved first then get the data using the json method. 
          const gameApiCall = await fetch('https://androidlessonsapi.herokuapp.com/api/game/details?game_id=' + this.gameId);
          const game = await gameApiCall.json();
          this.setState({game: game, loading: false});
      } catch(err) {
          console.log("Error fetching data-----------", err);
      }
  }

  onPressGoBack() {
    const action = {
      type: "LAST_PAGE_VIEWED",
      payload: this.state.game
    }

    this.props.dispatch(action);
    this.props.navigation.goBack();
  }

  render() {
    const { game, loading } = this.state;

    if(!loading) {
      const uri = game.url;
      return <View style={styles.container}>
                <Button onPress={() => this.onPressGoBack()} title="Go Back"/>

                <Text h1 style={styles.infoTitle} containerViewStyle={{width: '100%', marginLeft: 0}}>{game.name}</Text>

                <View>
                  <Text>{game.players}</Text>
                  <Text>{game.type}</Text>
                  <Text>{game.year}</Text>  
                </View>

                <Text>{game.description_en}</Text>
                <Text onPress={() => Linking.openURL(game.url)}>More details</Text>       
              </View>
    } else {
      return <ActivityIndicator />
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingLeft: 20,
    paddingRight: 20,
    backgroundColor: "#609dff",
   },
   infoTitle: {
    width: "100%",
    alignSelf: "center",
    marginTop: 22,
    marginBottom: 18,
    fontSize: 35,
    backgroundColor: "#f8f6ec",
    borderRadius: 15,
    overflow: "hidden",
    borderWidth: 0.5,
    textAlign: "center",
    marginBottom: 20,
  },
});

export default connect(undefined)(InfoScreen);


import React, {Component} from 'react';
// import textReducer from './Reducer';
import {createAppContainer, createStackNavigator} from 'react-navigation'
import {AsyncStorage, SafeAreaView, Text, Animated, View} from 'react-native';
import HomeScreen from './Home';
import InfoScreen from './Info';
import {Provider} from 'react-redux';
import { createStore } from 'redux';

class FadeInView extends React.Component {
  state = {
    fadeIn: new Animated.Value(0), 
    fadeOut: new Animated.Value(1),
  }

  componentDidMount() {
    Animated.timing(                  // Animate over time
      this.state.fadeIn,            // The animated value to drive
      {
        toValue: 1,                   // Animate to opacity: 1 (opaque)
        duration: 2000,              // Make it take a while
      }
    ).start(() => this.fadeOut());
  }

  fadeOut() {
    this.state.fadeIn.setValue(1)
    Animated.timing(                  
       this.state.fadeIn,            
       {
         toValue: 0,                   
         duration: 2000,              
       }
    ).start();     
  }

  render() {
    let { fadeIn } = this.state;

    return (
      <Animated.View                 // Special animatable View
        style={{
          ...this.props.style,
          opacity: fadeIn,         // Bind opacity to animated value
        }}
      >
        {this.props.children}
      </Animated.View>
    );
  }
}

const textReducer = (state = {}, action) => {
  switch(action.type) {
      case 'LAST_PAGE_VIEWED': 
          if(action.payload.id) {
            AsyncStorage.setItem('lastGameId', action.payload.name); 
          }
          return {
            ...state, 
            lastPage: action.payload.name || state.lastPage,
            lastGameId: action.payload.name || state.lastGameId
          }

      default: 
          return state
  }
}

//componentWillMount()

const store = createStore(textReducer)



const MainNavigator = createStackNavigator({
  Home: {screen : HomeScreen},
  Info: {screen: InfoScreen}
})

const App = createAppContainer(MainNavigator);

export default class Begin extends Component {
  state = {
    loading : true,
  }

  componentDidMount(){
    // Start counting when the page is loaded
    this.timeoutHandle = setTimeout(()=>{
         this.setState({loading: false})
    }, 5000);
  }

  render() {
    return (
      <Provider store={store}>
        {this.state.loading && 
          <FadeInView style={{width: 250, height: 50, backgroundColor: 'powderblue'}}>
            <Text style={{fontSize: 28, textAlign: 'center', margin: 10}}>Fading in</Text>
          </FadeInView>
        }
        {!this.state.loading && 
          <App />
        }
        
          {/* <App /> */}
      </Provider>
    );
  }
}

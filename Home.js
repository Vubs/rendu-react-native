import React, {Component} from 'react';
import { StyleSheet, Text, View, FlatList, AsyncStorage, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import { NavigationEvents } from "react-navigation";



function mapStateToProps(state) {
  return {
    lastPage: state.lastPage,
    lastGameId: state.lastGameId
  }
}

class HomeScreen extends Component {
  constructor(props) {
    super(props);

    // this.getDataFromLocalStorage()

    this.state = {
      apiGameData: [],
      lastGameStorage: '',
      loadingStorage: true,
    }
  }

  componentWillMount() {
    this.getDataFromLocalStorage()
  }

  // shouldComponentUpdate() {
  //   if(this.props.lastPage != null) {
  //     if (this.props.lastPage != this.state.lastGameId) {
  //       return true;
  //     }
  //   }
  // }

  async getDataFromLocalStorage() {
    try {
      const value = await AsyncStorage.getItem('lastGameId');
      if (value !== null) {
        this.setState({
          lastGameStorage: value,
          loadingStorage: false
        })
      }
    } catch (error) {
      console.log(error);
    }
  }


  async getGamesFromApi() {
    try {
      let response = await fetch(
        'https://androidlessonsapi.herokuapp.com/api/game/list',
      );
      let responseJson = await response.json();
      return responseJson;
    } catch (error) {
      console.error(error);
    }
  }

  async componentDidMount() {
    let data = await this.getGamesFromApi();
    this.setState({apiGameData: data});
  }

  render() {
    if(!this.state.loadingStorage) {
      return (
        <View style={styles.container}>
          <NavigationEvents
            onWillFocus={payload => {
              this.getDataFromLocalStorage()
            }}
          />   
          <Text h1 style={styles.homeTitle}>Hello Games</Text>
          
          <Text>{this.props.lastPage}</Text>
          <Text>{this.state.lastGameStorage}</Text> 

          <FlatList
            data={this.state.apiGameData}
            keyExtractor={(item, index) =>  item.id.toString()}
            renderItem={({item}) => <Text onPress={ () => {this.props.navigation.navigate('Info', {gameId: item.id} )} } key={item.id} style={styles.item}>{item.name}</Text>}
          />
          
        </View>
      )
    } else {
      return <ActivityIndicator />
    }
  }
}

const styles = StyleSheet.create({
  button1 : {
    flex: 1
  },
  button2: {
    flex: 1
  },
  container: {
    flex: 1,
    paddingLeft: 20,
    paddingRight: 20,
    backgroundColor: "#609dff",
   },
   item: {
     padding: 10,
     fontSize: 25,
     height: 70,
     marginTop: 15,
     marginBottom: 15,
     backgroundColor: "#f8f6ec",
     borderRadius: 15,
     overflow: "hidden",
     borderWidth: 0.5,
  },
   homeTitle: {
     alignSelf: "center",
     marginTop: 22,
     marginBottom: 18,
     fontSize: 35,
   },
});

export default connect(mapStateToProps)(HomeScreen);


